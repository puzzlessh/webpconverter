const chalk = require('chalk'); //v^4.1.2 - https://stackoverflow.com/questions/70309135/chalk-error-err-require-esm-require-of-es-module
import {Command} from 'commander';
import {basename, join, normalize} from 'node:path';
import {readdirSync, statSync} from 'node:fs';
const webp = require('webp-converter');
const isImage = require('is-image');

const program = new Command();

program
  .option(
    '-i, --inputDir <char>',
    'input directory',
    join(process.cwd(), 'convert')
  )
  .option(
    '-o, --outputDir <char>',
    'output directory',
    join(process.cwd(), 'webp')
  );

program.parse();

const OPTIONS = program.opts<{inputDir: string; outputDir: string}>();
const INPUT = normalize(OPTIONS.inputDir);
const OUTPUT = normalize(OPTIONS.outputDir);

function getPathFromDir(dir: string): string[] {
  const imagePaths: string[] = [];
  const result = [dir];

  while (result.length > 0) {
    const imagePath = result.shift()!;
    const statFile = statSync(imagePath);
    if (statFile.isDirectory()) {
      const innerPaths = readdirSync(imagePath).map(item =>
        join(imagePath, item)
      );
      result.push(...innerPaths);
      continue;
    }
    if (isImage(imagePath)) {
      imagePaths.push(imagePath);
    }
  }
  return imagePaths;
}

const allPicPath = getPathFromDir(INPUT);

function getFileName(filePath: string): string {
  const [name] = basename(filePath).split('.');
  if (!name) {
    throw new Error('Error path');
  }
  return name;
}

allPicPath.map(filePath => {
  const name = getFileName(filePath);
  const outputfilePath = normalize(OUTPUT + '/' + name + '.webp');
  const result = webp.cwebp(filePath, outputfilePath, '-q 80');
  return console.log(
    chalk.blue('FILE: ' + filePath + '   CONVERTED TO: ' + outputfilePath)
  );
});

console.log(chalk.green('---FILES HAVE BEEN SUCCESSFULLY CONVERTED---'));
