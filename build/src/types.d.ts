export declare enum ProcessStatus {
    Success = "success",
    Failed = "failed"
}
export interface Process {
    name: string;
    path: string;
    status: ProcessStatus;
}
export interface ProcessSuccess extends Process {
    name: string;
    path: string;
    status: ProcessStatus.Success;
}
export interface ProcessFailed extends Process {
    name: string;
    status: ProcessStatus.Failed;
    error: string;
}
export declare type ProcessResult = ProcessSuccess | ProcessFailed;
export interface ProcessDone {
    success: ProcessSuccess[];
    failed: ProcessFailed[];
}
export declare function isProcessSuccess(value: ProcessResult): value is ProcessSuccess;
export declare function isProcessFailed(value: ProcessResult): value is ProcessFailed;
export declare function processSuccess(name: string, path: string): ProcessSuccess;
export declare function processFailed(name: string, path: string, error: unknown): ProcessFailed;
export declare function processDone(results?: ProcessResult[]): ProcessDone;
