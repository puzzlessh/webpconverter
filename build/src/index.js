"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chalk = require('chalk'); //v^4.1.2 - https://stackoverflow.com/questions/70309135/chalk-error-err-require-esm-require-of-es-module
const commander_1 = require("commander");
const node_path_1 = require("node:path");
const node_fs_1 = require("node:fs");
const webp = require('webp-converter');
const isImage = require('is-image');
const program = new commander_1.Command();
program
    .option('-i, --inputDir <char>', 'input directory', (0, node_path_1.join)(process.cwd(), 'convert'))
    .option('-o, --outputDir <char>', 'output directory', (0, node_path_1.join)(process.cwd(), 'webp'));
program.parse();
const OPTIONS = program.opts();
const INPUT = (0, node_path_1.normalize)(OPTIONS.inputDir);
const OUTPUT = (0, node_path_1.normalize)(OPTIONS.outputDir);
function getPathFromDir(dir) {
    const imagePaths = [];
    let result = [dir];
    while (result.length > 0) {
        const imagePath = result.shift();
        const statFile = (0, node_fs_1.statSync)(imagePath);
        if (statFile.isDirectory()) {
            const innerPaths = (0, node_fs_1.readdirSync)(imagePath).map((item) => (0, node_path_1.join)(imagePath, item));
            result.push(...innerPaths);
            continue;
        }
        if (isImage(imagePath)) {
            imagePaths.push(imagePath);
        }
    }
    return imagePaths;
}
const allPicPath = getPathFromDir(INPUT);
function getFileName(filePath) {
    const [name] = (0, node_path_1.basename)(filePath).split('.');
    if (!name) {
        throw new Error('Error path');
    }
    return name;
}
allPicPath.map((filePath) => {
    const name = getFileName(filePath);
    const outputfilePath = (0, node_path_1.normalize)(OUTPUT + '/' + name + '.webp');
    const result = webp.cwebp(filePath, outputfilePath, "-q 80");
    return console.log(chalk.blue('FILE: ' + filePath + '   CONVERTED TO: ' + outputfilePath));
});
console.log(chalk.green('---FILES HAVE BEEN SUCCESSFULLY CONVERTED---'));
//# sourceMappingURL=index.js.map