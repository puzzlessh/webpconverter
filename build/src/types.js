"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processDone = exports.processFailed = exports.processSuccess = exports.isProcessFailed = exports.isProcessSuccess = exports.ProcessStatus = void 0;
var ProcessStatus;
(function (ProcessStatus) {
    ProcessStatus["Success"] = "success";
    ProcessStatus["Failed"] = "failed";
})(ProcessStatus = exports.ProcessStatus || (exports.ProcessStatus = {}));
function isProcessSuccess(value) {
    return value.status === ProcessStatus.Success;
}
exports.isProcessSuccess = isProcessSuccess;
function isProcessFailed(value) {
    return value.status === ProcessStatus.Failed;
}
exports.isProcessFailed = isProcessFailed;
function processSuccess(name, path) {
    return { name, path, status: ProcessStatus.Success };
}
exports.processSuccess = processSuccess;
function processFailed(name, path, error) {
    const message = error instanceof Error ? error.message : 'Unknown error';
    return { name, path, status: ProcessStatus.Failed, error: message };
}
exports.processFailed = processFailed;
function processDone(results = []) {
    const failed = results.filter(isProcessFailed);
    const success = results.filter(isProcessSuccess);
    return { failed, success };
}
exports.processDone = processDone;
//# sourceMappingURL=types.js.map